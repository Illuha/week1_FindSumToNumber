#include "FindSumToNumber.h"
#include <iostream>
#include <vector>
#include <algorithm>

int main() {
	int numCount;
	int * arr;
	cout << "How many numbers would you like to type? ";
	cin >> numCount;
	arr = new (nothrow) int[numCount];
	if (arr == nullptr)
		cout << "Error: memory could not be allocated";
	else
	{
		// input numbers
		for (int i = 0; i < numCount; i++)
		{
			cout << "Enter number: ";
			cin >> arr[i];
		}

		// input Sum-number
		int sum;
		cout << "Type sum-number: ";
		cin >> sum;

		findNumbers(arr, sum, numCount);
	}

	cin.get();// receive '\n' form input stream
	cin.get(); // wait for window close
}


void findNumbers(int *arr, int sum, int length)
{
	sort(arr, arr + length, reversCompareInt);

	int k = 0;
	for (int i = 0, j= length-1; i < j; ){
		k++;
		if ((arr[i] + arr[j]) > sum) { 
			i++;
		}
		else if ((arr[i] + arr[j]) < sum) { 
			j--; 
		}
		else if ((arr[i] + arr[j]) == sum){
			cout << "arg1 = " << arr[i] << ", arg2 = " << arr[j] << endl;
			break;
		}
	}
		

}


bool reversCompareInt(int i, int j) { return (j<i); }
bool compareInt(int i, int j) { return (i<j); }


//int main(int argc, char** argv) {
//
//	vector<int> cVector;
//	vector<int>::iterator it;
//	it = cVector.begin();
//
//	
//	cout << " argc =" << argc << endl;
//	for (int i = 1; i < argc; i++){
//		cout << "param # "<< i << "= " << argv[i] << endl;
//		it = cVector.insert(it, atoi(argv[i]));
//	}
//
//	
//	print_vector(&cVector);
//	cout << endl;
//
//	
//	// find sum
//	int n;
//	cout << "Please enter an integer value: ";
//	cin >> n;
//	
//	findNumbers( &cVector, n);
//
//	cin.get();// receive '\n' form input stream
//	cin.get(); // wait for window close
//
//	return 0;
//
//}
//
//void findNumbers(vector<int> *vec, int sumNum) {
//
//	cout << "Your input: " << sumNum << endl;
//	// sort revers
//	sort(vec->rbegin(), vec->rend());
//	print_vector(vec);
//
//	cout << endl;
//
//	vector<int>::iterator it;
//	it = vec->begin();
//	for (it = vec->begin(); it != vec->end(); ++it) {
//		if(*it >= sumNum) continue;// we need less then sumNum
//		cout << "less then sumNum: " << *it << endl;
//	}
//
//	
//}
//
//void print_vector(vector<int> *vec) {
//	vector<int>::iterator it;
//	it = vec->begin();
//	for (it = vec->begin(); it != vec->end(); ++it)
//		cout << *it << ' ';
//}
